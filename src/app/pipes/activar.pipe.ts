import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'activar'
})
export class ActivarPipe implements PipeTransform {

  transform(value: string, mostrar:boolean = true): string {
    
    /* let asterisco:string = '';
    if (mostrar === false) {
      
      for (let i = 0; i < value.length; i++) {
        console.log(i);
        asterisco += '*';
        
      }
      return asterisco;
    }
    else {
      return value;
    } */
    return ( mostrar === false ) ? '*'.repeat( value.length ):value;

  }

}
