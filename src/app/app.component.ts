import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  nombre: string = 'Capitan America';

  nombre2 : string= 'AaRon LoPezZ'

  arreglo = [1,2,3,4,5,6,7,8,9,10];
  
  PI:number = Math.PI;

  porcentaje : number = 0.214;

  salario : number= 12345;

  heroe: Object = {
    nombre: 'logam',
    clave: 'Wolverine',
    edad: 500,
    direccion: {
      clave: 'Primera',
      casa: 20
    }
  };

  valorPromesa = new Promise<string>( (resolve, reject) => {
    setTimeout(() => {
      resolve('llego la data');
    }, 4500);
  });

  fecha: Date = new Date();

  idioma : string = 'fr';

  videoURL: string = 'https://www.youtube.com/embed/TL0_caxDjG8';

  activar : boolean = false;
  // changeLenguaje(data:string){
  //   return this.idioma = data;
  // }
}
